
function scopesValidation(allowScopes){

    return function (req,res,next){

        if (!req.userData || !req.userData.scopes){            
            return res.error({
                message:'Unauthorized.'
            },401)
        }

        // the admin role has all permissions
        if (req.userData.role_id === 1){
            next()
        }
        else{

            const permission = allowScopes.map((scope) => {
                return req.userData.scopes.includes(scope)       
            })

            const hasAccess = !permission.includes(false)


            if (hasAccess){
                next()
            }
            else{
                return res.error({
                    message:'Unauthorized.'
                },401)
            }
        }
        
    }
}

module.exports = scopesValidation