require('dotenv').config()

const config = {
    port:process.env.PORT || 8000,
    dbHost:process.env.DB_HOST,
    dbName:process.env.DB_NAME,
    dbUser:process.env.DB_USER,
    dbPassword:process.env.DB_PWD,
    dbPort:process.env.DB_PORT,
    dbDialect:process.env.DB_DIALECT,    
    authJwtSecret:process.env.AUTH_JWT_SECRET
}

module.exports = { config }