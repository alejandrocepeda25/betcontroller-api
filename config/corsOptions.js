const { config } = require('./index')

let corsOptions = {}

if (config.modeEnv === 'production'){
    corsOptions = {
        origin : '*'
    }
}

module.exports = corsOptions