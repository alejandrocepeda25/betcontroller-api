'use strict'

const path = require('path')
const route = require('express').Router()

route.use('/users', require(path.join(__dirname, '/user')))

/*
route.use('/roles', require(path.join(__dirname, '/role')))
route.use('/roles-users', require(path.join(__dirname, '/roleUsers')))
route.use('/permissions', require(path.join(__dirname, '/permission')))
*/


module.exports = route