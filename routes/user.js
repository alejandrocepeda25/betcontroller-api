'use strict'

const route = require('express').Router()

const checkAuth = require('../middlewares/checkAuth')
const scopesValidation = require('../middlewares/scopesValidation')

// User Controller
/*
const userController = require('../controllers/user/userController')

route.post('/login', userController.login)
route.post('/register',[checkAuth,scopesValidation(['create:users'])], userController.store)

route.get('/',  [checkAuth,scopesValidation(['show:users'])], userController.index)
route.get('/:id', [checkAuth,scopesValidation(['show:users'])], userController.show)

route.delete('/:id', [checkAuth,scopesValidation(['destroy:users'])], userController.destroy)
route.put('/:id', [checkAuth,scopesValidation(['update:users'])], userController.update)
*/
module.exports = route