const express = require('express')
const server = express()
const morgan = require('morgan')
const { config } = require('./config')
const cors = require('cors')
const bodyParser = require('body-parser')

server.use(morgan('tiny'))
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({limit: '900mb', extended: true}));
server.use(bodyParser.json({limit: '900mb'}))
server.use(require('./middlewares/responder'))

server.use(cors(require('./config/corsOptions')));



server.use('/api', require('./routes/api'))

server.listen(config.port,() => {
    console.log('====================================');
    console.log(`Server is listening on port ${config.port}`);
    console.log('====================================');
})